package cn.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/*
 * 线程类
 */
public class Processor extends Thread{
	private Socket socket;
	private InputStream inputStream;
	private PrintStream out;
	public final static String base_url="C:\\Users\\zhuhaitao\\workspace\\ApacheServer\\thdoc";
	public Processor(Socket socket){
		this.socket=socket;
		try {
			this.inputStream=socket.getInputStream();
			this.out=new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		System.out.println("。。。。请求开始。。。。");
		String fineName=parse(inputStream);
		sendFile(fineName);
	}
     /**
      * 解析http文件头信息
      */
	public String parse(InputStream in){
		BufferedReader br=new BufferedReader(new InputStreamReader(in));
		String fileName=null;
		try {
			String httpMassage=br.readLine();
			String[] content=httpMassage.split(" ");
			if(content.length!=3){
				sendErrorMessage(400,"client query  error!");
				return null;
			}
			System.out.println("code:="+content[0]+",fileName:="+content[1]+",http-version:="+content[2]);
			fileName=content[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return fileName;
	}
	/**
	 * 发送错误的信息
	 */
	public void sendErrorMessage(int errorCode,String errorMessage){
		out.println("HTTP/1.0 "+errorCode+" "+errorMessage);
		out.println("content-type:text/html");
		out.println();
		out.println("<html>");
		out.println("<title> Error message</title>");
		out.println("<body>");
		out.println("<h1> errorCode="+errorCode+",errorMessage="+errorMessage+"</h1>");
		out.println("</body>");
		out.println("</html>");
		out.flush();
		out.close();
	}
	/**
	 * 发送文件
	 * @param fileName
	 */
	public void sendFile(String fileName){
		File file=new File(base_url+fileName);
		System.out.println(file.getPath());
		if(!file.exists()){
			sendErrorMessage(404,"File not found!");
			return;
		}
		try {
			InputStream in=new FileInputStream(file);
			byte content[]=new byte[(int)file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 queryFile");
			out.println("content-type:text/html; charset=uft-8");
			out.println("content-length:"+content.length);
			out.println();
			out.write(content);
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
