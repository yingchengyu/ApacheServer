package cn.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * web  服务端
 * @author Administrator
 *
 */
public class WedServer {
	
	//服务端启动 
	public void serverStart(){
		try {
			ServerSocket serverSocket=new ServerSocket(8081);
			//一直监听
			while(true){
				Socket socket=serverSocket.accept();
				new Processor(socket).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
   public static void main(String[] args) {
	   new WedServer().serverStart();
   }
}
